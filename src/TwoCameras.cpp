#include <osg/Node>
#include <osg/Group>
#include <osg/Geode>
#include <osg/Geometry>
#include <osg/Texture2D>
#include <osgDB/ReadFile>
#include <osgViewer/Viewer>
#include <osg/PositionAttitudeTransform>
#include <osgGA/TrackballManipulator>

#include <CameraConfig.h>

Producer::CameraConfig* setupCameras()
{
  	// Declare a camera configuration instance..
	Producer::CameraConfig* myCameraConfig = new Producer::CameraConfig();

  	// .. and two camera instances. A camera to follow the tank...
  	Producer::Camera* tankFollowerCamera = new Producer::Camera();
  	// .. and a traditional view of the scene
	Producer::Camera* mainCamera = new Producer::Camera();

  	// Set the parameters for the main camera.
  	// Projection rectangle describes the area of the render surface
  	// this view will occupy. Arguments are (left, right, bottom, top)
	mainCamera->setProjectionRectangle(0.25f, 0.75f, 0.0f, 0.5f);

  	// We don't want this camera to share a lens with other cameras.
	mainCamera->setShareLens(false);

  	// Let producer determine the fov based on the projection rectangle
	mainCamera->getLens()->setAutoAspect(true);

  	// Similar calls for the tank follower camera
	tankFollowerCamera->setProjectionRectangle(0.1f, 0.9f, 0.55f, 0.95f);
	tankFollowerCamera->setShareLens(false);
	tankFollowerCamera->getLens()->setAutoAspect(true);

  	// Add the cameras to our camera configuration, we'll use the name
  	// to retrieve an handle to the camera later.
	myCameraConfig->addCamera("tankFollowerCamera",tankFollowerCamera);
	myCameraConfig->addCamera("mainCamera",mainCamera);

  	// We want both cameras to use the same render surface. Retrieve a
  	// handle to the default render surface...
	Producer::RenderSurface* rsOne = mainCamera->getRenderSurface();

  	// .. and assign that to the second camera.
	tankFollowerCamera->setRenderSurface( rsOne );

	return myCameraConfig;
 }

int main( int argc, char **argv )
{
	// pointers to the root node and the tank model node
	Producer::ref_ptr<osg::Group> rootNode;
	Producer::ref_ptr<osg::Group> ownTank;

   	// build scene with terrain and two tanks
	if (!setupScene(rootNode,ownTank))
	{
		std::cout<< "problem setting up scene" << std::endl;
		return -1;
	}

   	// Declare a camera configuration instance, initialize it using
   	// the function we defined earlier.
	Producer::CameraConfig* myCameraConfig = setupCameras();

   	// Use this camera configuration instance to set up the viewer
	osgProducer::Viewer viewer(myCameraConfig);
	viewer.setUpViewer(osgProducer::Viewer::STANDARD_SETTINGS);
	viewer.setSceneData( rootNode.get() );

   	// Declare a transform for a position above and behind the tank
   	// aimed down slightly. Add this transform to the tank node.
	osg::PositionAttitudeTransform * followerOffset = new osg::PositionAttitudeTransform();
	followerOffset->setPosition( osg::Vec3(0.0,-25.0,10) );
	followerOffset->setAttitude( osg::Quat( osg::DegreesToRadians(-15.0), osg::Vec3(1,0,0) ) );
	ownTank.get()->addChild(followerOffset);

   	// Declare a transform accumulator to be used to position a
   	// camera. Attach transform accumulator to the follower transform.
	transformAccumulator* tankFollowerWorldCoords = new transformAccumulator();
	tankFollowerWorldCoords->attachToGroup(followerOffset);

   	// create the windows and run the threads.
	viewer.realize();
	while( !viewer.done() )
	  {
	   // wait for all cull and draw threads to complete.
	   viewer.sync();

	   // update the scene by traversing it with the the update visitor which will
	   // call all node update callbacks and animations.
	   viewer.update();

	   // Get a handle to the follower camera. Use camera class method to set
	   // the matrix to the world coordinate position of our tank follower.
	   // Rotate the matrix from Y up to Z up.
	   viewer.getCameraConfig()->
		  findCamera("tankFollowerCamera")->setViewByMatrix(
		  Producer::Matrix(tankFollowerWorldCoords->getMatrix().ptr() ) *
		  Producer::Matrix::rotate( -M_PI/2.0, 1, 0, 0 ) );

	   // fire off the cull and draw traversals of the scene.
	   viewer.frame();
	}

	// wait for all cull and draw threads to complete before exit.
	viewer.sync();

	return 0;
}
